package TestFiles;

import MatchesData.CsvFileRead;
import Problems.Problem1;
import org.junit.Test;

import static org.testng.AssertJUnit.*;

public class Problem1Test {

    @Test
    public void testProblem1(){
        var obj = new Problem1();
        CsvFileRead ob= new CsvFileRead();
        ob.readData();

        String str = "[2009=57, 2008=58, 2017=59, 2015=59, 2016=60, 2010=60, 2014=60, 2011=73, 2012=74, 2013=76]";
        assertEquals(str,obj.matchesPerYear());
        assertFalse(str== obj.matchesPerYear());
        assertNotNull(str,obj.matchesPerYear());
        assertNull(null);
//        assertNull(obj.matchesPerYear());

    }

}