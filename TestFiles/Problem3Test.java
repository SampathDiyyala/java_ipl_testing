package TestFiles;

import DeliveriesData.DelveriesCsvRead;
import MatchesData.CsvFileRead;
import Problems.Problem3;
import org.junit.Test;

import static org.testng.AssertJUnit.*;

public class Problem3Test {
    @Test
    public void testProblem3(){
        CsvFileRead ob= new CsvFileRead();
        ob.readData();
        DelveriesCsvRead del = new DelveriesCsvRead();
        del.readDelData();
        Problem3 obj3 = new Problem3();
        String str = "{Gujarat Lions=129, Mumbai Indians=131, Sunrisers Hyderabad=157, Kings XI Punjab=112, Delhi Daredevils=140, Rising Pune Supergiants=124, Kolkata Knight Riders=143, Royal Challengers Bangalore=152}";
        assertEquals(obj3.extraRuns2016(),str);
        assertFalse(str== obj3.extraRuns2016());
        assertNotNull(str,obj3.extraRuns2016());
        assertNull(null);
    }

}