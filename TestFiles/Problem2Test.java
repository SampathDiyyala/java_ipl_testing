package TestFiles;

import MatchesData.CsvFileRead;
import Problems.Problem2;
import org.junit.Test;

import static org.testng.AssertJUnit.*;

public class Problem2Test {
    @Test
    public void testProblem2(){
        Problem2 obj2 = new Problem2();
        CsvFileRead ob= new CsvFileRead();
        ob.readData();
        assertEquals("36",obj2.matchesWonPerYear());
        assertFalse("14"==obj2.matchesWonPerYear());
        assertNotNull("",obj2.matchesWonPerYear());

    }

}