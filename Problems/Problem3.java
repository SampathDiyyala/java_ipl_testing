package Problems;

import DeliveriesData.Deliveries;
import DeliveriesData.DelveriesCsvRead;
import MatchesData.CsvFileRead;
import MatchesData.Match;

import java.util.*;

public class Problem3 {
    ListIterator<Deliveries> delit = DelveriesCsvRead.deliverisData.listIterator();
    ListIterator<Match> matchit = CsvFileRead.matchesData.listIterator();


//    private HashSet teams = new HashSet();
    private ArrayList<Integer> matchId = new ArrayList<>();
    private HashMap<String,Integer> extraRuns2016 = new HashMap<>();

    public String extraRuns2016(){
        int extraRuns = 0;

        while (matchit.hasNext())
        {
//            System.out.println(delit.next().getMatchId());
            if(matchit.next().getSeason()==2016) {
                matchId.add(matchit.next().getMatchId());
            }
//            teams.add(matchit.next().getTeam1());
        }
        while (delit.hasNext()){
           try
           {
               if(matchId.contains(delit.next().getMatchId())){
                   extraRuns += delit.next().getExtraRuns();
//                   System.out.println(extraRuns);
                   extraRuns2016.put(delit.next().getBowlingTeam(),extraRuns);
               }
           }
           catch (NoSuchElementException e){}

        }
//        System.out.println(extraRuns2016);
        Object o = extraRuns;
        return extraRuns2016.toString();

    }

}
