package Problems;

import java.util.*;

public class Problem1 {
    private ArrayList<Integer> seasonList = new ArrayList<>();
    private HashMap<Integer,Integer> result = new HashMap<>();
    public String matchesPerYear()
    {
        Seasons seasons = new Seasons();
        seasonList = seasons.getSeasons();
        int s1,s2,s3,s4,s5,s6,s7,s8,s9,s10;
        s1=2008;
        s2=2009;
        s3=2010;
        s4=2011;
        s5=2012;
        s6=2013;
        s7=2014;
        s8=2015;
        s9=2016;
        s10=2017;
        int arr[]=new int[10];
        for(int season : seasonList){
            if(season==s1){
                ++arr[0];
                result.put(season,arr[0]);
            }
            else if(season==s2){
                ++arr[1];
                result.put(season,arr[1]);
            }
            else if(season==s3){
                ++arr[2];
                result.put(season,arr[2]);

            }
            else if(season==s4){
                ++arr[3];
                result.put(season,arr[3]);

            }
            else if(season==s5){
                ++arr[4];
                result.put(season,arr[4]);

            }
            else if(season==s6){
                ++arr[5];
                result.put(season,arr[5]);

            }
            else if(season==s7){
                ++arr[6];
                result.put(season,arr[6]);

            }
            else if(season==s8){
                ++arr[7];
                result.put(season,arr[7]);

            }
            else if(season==s9){
                ++arr[8];
                result.put(season,arr[8]);

            }
            else if(season==s10){
                ++arr[9];
                result.put(season,arr[9]);

            }
        }
//        System.out.println(result);
       List<Map.Entry<Integer,Integer>> sortedList = new ArrayList<>(result.entrySet());
        Collections.sort(sortedList, Comparator.comparing(Map.Entry::getValue));
//        System.out.println(sortedList.toString().getClass().getSimpleName());
        return sortedList.toString();
    }
}
