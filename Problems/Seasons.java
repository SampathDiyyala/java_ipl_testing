package Problems;

import MatchesData.CsvFileRead;
import MatchesData.Match;

import java.util.ArrayList;
import java.util.ListIterator;

public class Seasons {

    public ArrayList<Integer> seasonList = new ArrayList<Integer>();

    public ArrayList<Integer> getSeasons() {

        ListIterator<Match> it = CsvFileRead.matchesData.listIterator();

        while (it.hasNext()) {

            seasonList.add(it.next().getSeason());

        }
        return  seasonList;
    }
}

//class Teams
//{
//    public Set<String> teamSet = new HashSet<String>();
//
//    public Set<String> getTeams() {
//
//        ListIterator<Match> it = CsvFileRead.matchesData.listIterator();
//
//        while (it.hasNext()) {
//
//            teamSet.add(it.next().getTeam1());
//
//        }
//        return teamSet;
//
//    }
//
//}





