package MatchesData;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvFileRead {
    public static List<Match> matchesData = new ArrayList<>();
    public void readData() {
        String csvPath = "/home/sampath/JAVA/JAVA_IPL_Project/src/matches.csv";
        String lines="";
        BufferedReader read = null;
        int skip = 0;
        try
        {
            read = new BufferedReader(new FileReader(csvPath));
            while((lines = read.readLine())!=null)
            {
                if(skip == 0)
                {
                    skip++;
//                    System.out.println(lines);
                    continue;
                }
//                System.out.println(lines);
                String[] line = lines.split(",");
//                System.out.println(lines);
                this.setData(line);
            }
            read.close();
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }
    public void setData(String[] data)
    {

        Match match = new Match();
        int data0 = Integer.parseInt(data[0]);
        int data1 = Integer.parseInt(data[1]);

        int data11 = Integer.parseInt(data[11]);
        int data12 = Integer.parseInt(data[12]);
//        System.out.println(((Object)data1));
        match.setMatchId(data0);
        match.setSeason(data1);
        match.setCity(data[2]);
        match.setDate(data[3]);
        match.setTeam1(data[4]);
        match.setTeam2(data[5]);
        match.setTossWinner(data[6]);
        match.setTossDecision(data[7]);
        match.setResult(data[8]);
        match.setWinner(data[10]);
        match.setWinByRuns(data11);
        match.setWinByWickets(data12);
        match.setPlayerOfMatch(data[13]);
        matchesData.add(match);
//        System.out.println(match);
//        for(int i = 0;i<matchesData.size();i++) {
//            System.out.println(matchesData.get(i).getSeason() +" : "+ matchesData.get(i).getPlayerofmatch());
//        }

    }

}
