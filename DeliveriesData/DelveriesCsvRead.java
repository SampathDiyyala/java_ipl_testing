package DeliveriesData;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DelveriesCsvRead {
    public static List<Deliveries> deliverisData = new ArrayList<>();
    public void readDelData() {
        String csvPath = "/home/sampath/JAVA/JAVA_IPL_Project/src/deliveries.csv";
        String lines="";
        BufferedReader read = null;
        int skip = 0;
        try
        {
            read = new BufferedReader(new FileReader(csvPath));
            while((lines = read.readLine())!=null)
            {
                if(skip == 0)
                {
                    skip++;
//                    System.out.println(lines);
                    continue;
                }
//                System.out.println(lines);
                String[] line = lines.split(",");
//                System.out.println(lines);
                this.setDelData(line);
            }
            read.close();
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }
    public void setDelData(String[] data)
    {

        Deliveries deliver = new Deliveries();
        int data0 = Integer.parseInt(data[0]);
        int data1 = Integer.parseInt(data[1]);
//        int data11 = Integer.parseInt(data[11]);
//        int data12 = Integer.parseInt(data[12]);
//        System.out.println(((Object)data0));
        deliver.setMatchId(data0);
        deliver.setInning(data1);
        deliver.setBattingTeam(data[2]);
        deliver.setBowlingTeam(data[3]);
        deliver.setOver(Integer.parseInt(data[4]));
        deliver.setBall(Integer.parseInt(data[5]));
        deliver.setExtraRuns(Integer.parseInt(data[16]));
        deliver.setBowler(data[8]);
        deliver.setTotalRuns(Integer.parseInt(data[17]));

        deliverisData.add(deliver);
//        System.out.println(deliverisData.get(0));

    }

}
