package DeliveriesData;

public class Deliveries {
    private int delId;
    private int inning;
    private String battingTeam;
    private String bowlingTeam;
    private int over;
    private int ball;
    private String batsman;
    private String nonStriker;
    private String bowler;
    private  int superover;
    private int widdeRuns;
    private int byeRuns;
    private int legByeRuns;
    private int noballRuns;
    private int penaltyRuns;
    private int batsmanRuns;
    private int extraRuns;
    private int totalRuns;
    private String playerDismissed;
    private  String dismissalKind;
    private String fielder;

    public void setMatchId(int delId){
        this.delId=delId;
    }
    public void setInning(int inning){
        this.inning = inning;
    }
    public void setBattingTeam(String battingTeam){
        this.battingTeam = battingTeam;
    }
    public void setBowlingTeam(String bowlingTeam){
        this.bowlingTeam = bowlingTeam;
    }
    public void setOver(int over){
        this.over = over;
    }
    public void setBall(int ball){
        this.ball = ball;
    }
    public void setBatsman(String batsman){
        this.batsman = batsman;
    }
    public void setNonStriker(String nonStriker){
        this.nonStriker = nonStriker;
    }
    public void setBowler(String bowler){
        this.bowler = bowler;
    }
    public void setSuperover(int superover){
        this.superover = superover;
    }
    public void setWiddeRuns(int widdeRuns){
        this.widdeRuns = widdeRuns;
    }
    public void setByeRuns(int byeRuns){
        this.byeRuns = byeRuns;
    }
    public void setLegByeRuns(int legByeRuns){
        this.legByeRuns = legByeRuns;
    }
    public void setNoballRuns(int noballRuns){
        this.noballRuns = noballRuns;
    }
    public void setPenaltyRuns(int penaltyRuns){
        this.penaltyRuns = penaltyRuns;
    }
    public void setBatsmanRuns(int batsmanRuns){
        this.batsmanRuns = batsmanRuns;
    }
    public void setExtraRuns(int extraRuns){
        this.extraRuns = extraRuns;
    }
    public void setTotalRuns(int totalRuns){
        this.totalRuns = totalRuns;
    }
    public void setPlayerDismissed(String playerDismissed){
        this.playerDismissed = playerDismissed;
    }
    public void setDismissalKind(String dismissalKind){
        this.dismissalKind = dismissalKind;
    }
    public void setFielder(String fielder){
        this.fielder = fielder;
    }

    public int getBall() {
        return ball;
    }

    public int getBatsmanRuns() {
        return batsmanRuns;
    }

    public int getByeRuns() {
        return byeRuns;
    }

    public int getMatchId() {
        return delId;
    }

    public int getExtraRuns() {
        return extraRuns;
    }

    public int getInning() {
        return inning;
    }

    public int getLegByeRuns() {
        return legByeRuns;
    }

    public int getNoballRuns() {
        return noballRuns;
    }

    public int getOver() {
        return over;
    }

    public int getPenaltyRuns() {
        return penaltyRuns;
    }

    public int getSuperover() {
        return superover;
    }

    public int getTotalRuns() {
        return totalRuns;
    }

    public int getWiddeRuns() {
        return widdeRuns;
    }

    public String getBatsman() {
        return batsman;
    }

    public String getBattingTeam() {
        return battingTeam;
    }

    public String getBowler() {
        return bowler;
    }

    public String getBowlingTeam() {
        return bowlingTeam;
    }

    public String getDismissalKind() {
        return dismissalKind;
    }

    public String getFielder() {
        return fielder;
    }

    public String getNonStriker() {
        return nonStriker;
    }

    public String getPlayerDismissed() {
        return playerDismissed;
    }

}
